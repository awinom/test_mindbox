﻿using System;
using ShapeCalculator.Interface;

namespace ShapeCalculator.Shape
{
    /// <summary>
    /// Круг
    /// </summary>
    public class Circle : IShape
    {
        /// <summary>
        /// Радиус круга
        /// </summary>
        private readonly double _radius;

        /// <summary>
        /// Конструктор принимает радиус круга
        /// </summary>
        /// <param name="radius">Радиус круга</param>
        public Circle(double radius)
        {
            if (radius < 0)  throw new ArgumentException("Радиус не может быть меньше 0");
            _radius = radius;
        }

        /// <summary>
        /// Возвращает площадь круга
        /// </summary>
        public double GetArea() 
        { 
            return Math.PI * Math.Pow(_radius, 2); 
        }
        
    }
}