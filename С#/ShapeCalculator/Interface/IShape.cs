﻿namespace ShapeCalculator.Interface

{
    /// <summary>
    /// Интерфейс фигуры
    /// </summary>
    public interface IShape
    {
        double GetArea();
    }

    /// <summary>
    /// Интерфейс калькулятора фигуры
    /// </summary>
    public interface IShapeCalculator
    {
        double CalculateAreaShape(IShape shape);
    }

}
