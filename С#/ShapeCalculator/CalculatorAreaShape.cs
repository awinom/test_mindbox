﻿using ShapeCalculator.Interface;

namespace ShapeCalculator
{
    /// <summary>
    /// Калькулятор фигуры
    /// </summary>
    public class CalculatorAreaShape : IShapeCalculator
    {
        /// <returns>Возвращает площадь фигуры без знания типа фигуры в compile-time</returns>
        public double CalculateAreaShape(IShape shape)
        {
            return shape.GetArea();
        }
    }
}
