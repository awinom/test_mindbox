using Xunit;
using ShapeCalculator.Shape;
using ShapeCalculator.Interface;

namespace ShapeCalculator.Tests
{
    public class TriangleTests
    {


        /// <summary>
        /// �������� �� ���������� ������� ������������ �� ���� ��������
        /// (������������� ��������)
        /// </summary>
        [Theory]
        [InlineData(8, 9, 10, 34.197039345533994)]
        [InlineData(223, 341, 204, 21875.048799945565)]
        [InlineData(12342, 5706, 8943, 23495411.58496486)]
        public void GetArea_IntSide_CheckClculatorAreaTriangle_ReturnArea(int side_1, int side_2, int side_3, double expected)
        {
            // arrange
            IShape Shape = new Triangle(side_1, side_2, side_3);

            // act
            var actual = Shape.GetArea();

            // assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// �������� �� ���������� ������� ������������ �� ���� ��������
        /// (������� ��������)
        /// </summary>
        [Theory]
        [InlineData(0.0032, 0.0103, 0.00801, 0.000010073938945336881)]
        [InlineData(10.5, 13.503, 16.0052, 70.303784210772804)]        
        [InlineData(3344.20401, 5324.682007, 3823.9350381, 6362248.485918389 )] //6362248.4859183915
        public void GetArea_DoubleSide_CheckClculatorAreaTriangle_ReturnArea(double side_1, double side_2, double side_3, double expected)
        {
            // arrange
            IShape Shape = new Triangle(side_1, side_2, side_3);

            // act
            var actual = Shape.GetArea();

            // assert
            Assert.Equal(expected, actual, 1E-7);
        }

        /// <summary>
        /// �������� �� ������������ ������
        /// (�� ������������� ��������)
        /// </summary>
        [Theory]
        [InlineData(0, 0, 0)]
        [InlineData(-3, 4, 5)]
        [InlineData(3, -4.02023, 5)]
        [InlineData(2, 4, -5.9240)]
        [InlineData(-9940, 8424.23019, -8930.9240)]
        [InlineData(-346.1145, -424.92502, -396.42378064)]
        public void GetArea_MinusSide_CheckClculatorAreaTriangle_ReturnError(double side_1, double side_2, double side_3)
        {
            // arrange
            IShape Shape;

            // act
            var exception = Assert.Throws<ArgumentException>(() => Shape = new Triangle(side_1, side_2, side_3));

            // assert
            Assert.Equal("������� ������ ���� ������ 0", exception.Message);

        }

        /// <summary>
        /// �������� �� ������������ ������
        /// (����� 2 ������ ������ 3 �������)
        /// </summary>
        [Theory]
        [InlineData(5, 7.231001, 215)]
        [InlineData(45.20529, 381, 51.00203)]
        [InlineData(295.45, 4.053001, 75.03924)]
        public void GetArea_BigDiffSide_CheckClculatorAreaTriangle_ReturnError(double side_1, double side_2, double side_3)
        {
            // arrange
            IShape Shape;

            // act
            var exception = Assert.Throws<ArgumentException>(() => Shape = new Triangle(side_1, side_2, side_3));

            // assert
            Assert.Equal("����� ����� ���� ������ ������������ ������ ���� ������ ������� �������", exception.Message);

        }

        /// <summary>
        /// ���� �������� �� ��, �������� �� ����������� ������������� 
        /// (������������� ��������)
        /// </summary>
        [Theory]
        [InlineData(3, 4, 5, true)]
        [InlineData(100, 100, 100, false)]
        [InlineData(50, 30, 58, false)]
        public void IsRectangular_IntSide_CheckRectangularTriangle_Returnbool(int side_1, int side_2, int side_3, bool expected)
        {
            // arrange
            Triangle Tri = new Triangle(side_1, side_2, side_3);

            // act
            var actual = Tri.IsRectangular();

            // assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// ���� �������� �� ��, �������� �� ����������� ������������� 
        /// (������� ��������)
        /// </summary>
        [Theory]
        [InlineData(3.00, 4.00000, 5.0, true)]
        [InlineData(362.9283, 249.23567, 125.63920484, false)]
        [InlineData(50, 30, 58.309518948453, true)]
        [InlineData(50, 30, 58.3095189489, true)]
        [InlineData(50, 30, 58.3095189482, true)]
        [InlineData(50, 30, 58.309518947, false)]
        [InlineData(50, 30, 58.309518949, false)]
        public void IsRectangular_DoubleSide_CheckRectangularTriangle_Returnbool(double side_1, double side_2, double side_3, bool expected)
        {
            // arrange
            Triangle Tri = new Triangle(side_1, side_2, side_3);

            // act
            var actual = Tri.IsRectangular();

            // assert
            Assert.Equal(expected, actual);
        }
    }
}