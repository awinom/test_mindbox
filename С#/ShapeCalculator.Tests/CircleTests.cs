﻿using Xunit;
using ShapeCalculator.Shape;
using ShapeCalculator.Interface;

namespace ShapeCalculator.Tests
{
    public class CircleTests
    {
        /// <summary>
        /// Проверка на вычисление площади круга по радиусу
        /// (целочисленные значения)
        /// </summary>
        [Theory]
        [InlineData(0, 0)]
        [InlineData(10, 314.15926535897933)]
        [InlineData(79, 19606.6797510539)]
        [InlineData(561, 988725.18153043324)]
        public void GetArea_IntRadius_CheckClculatorAreaCircle_ReturnArea(int radius, double expected)
        {
            // arrange
            IShape Shape = new Circle(radius);

            // act
            var actual = Shape.GetArea();

            // assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Проверка на вычисление площади круга по радиусу
        /// (дробные значения)
        /// </summary>
        [Theory]
        [InlineData(0.00052, 8.4948665353067985E-07)]
        [InlineData(19.930552, 1247.9251203506351)]
        [InlineData(145.345378, 66367.021016008439)]
        [InlineData(3892.00424, 47587893.627129659)]
        public void GetArea_DoubleRadius_CheckClculatorAreaCircle_ReturnArea(double radius, double expected)
        {
            // arrange
            IShape Shape = new Circle(radius);

            // act
            var actual = Shape.GetArea();

            // assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Проверка на корректность ошибки
        /// (отрицательные значения)
        /// </summary>
        [Theory]
        [InlineData(-0.00001)]
        [InlineData(-45.20529)]
        [InlineData(-29545.45)]
        public void GetArea_MinusRadius_CheckClculatorAreaCircle_ReturnError(double radius)
        {
            // arrange
            IShape Shape;

            // act
            var exception = Assert.Throws<ArgumentException>(() => Shape = new Circle(radius));

            // assert
            Assert.Equal("Радиус не может быть меньше 0", exception.Message);

        }

    }
}
